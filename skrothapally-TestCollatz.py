#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
    
    def test_read_2(self):
        s = "1 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 999999)

    def test_read_3(self):
        s = "1 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1)
    
    def test_read_4(self):
        s = "500 1500\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  500)
        self.assertEqual(j, 1500)
            
    # ----
    # helper
    # ----

    def test_helper_1(self):
        v = collatz(1)
        self.assertEqual(v, 1)

    def test_helper_2(self):
        v = collatz(999999)
        self.assertEqual(v, 259)
    
    def test_helper_3(self):
        v = collatz(854532)
        self.assertEqual(v, 145)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_2(self):
        v = collatz_eval(899999, 900001)
        self.assertEqual(v, 225)

    def test_eval_3(self):
        v = collatz_eval(8000, 8001)
        self.assertEqual(v, 115)

    def test_eval_4(self):
        v = collatz_eval(5999, 6000)
        self.assertEqual(v, 187)
    
    def test_eval_5(self):
        v = collatz_eval(60500, 8493)
        self.assertEqual(v, 340)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 50, 50, 25)
        self.assertEqual(w.getvalue(), "50 50 25\n")
    
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 999999, 525)
        self.assertEqual(w.getvalue(), "1 999999 525\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 10000, 46054, 324)
        self.assertEqual(w.getvalue(), "10000 46054 324\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_2(self):
        r = StringIO("1 1\n1 999999\n999999 999999\n6000 6001\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n1 999999 525\n999999 999999 259\n6000 6001 50\n")

    def test_solve_3(self):
        r = StringIO("999999 1\n94234 30232\n400001 399999\n5000 4999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "999999 1 525\n94234 30232 351\n400001 399999 229\n5000 4999 91\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
